/**
 * Bài tập: Quản lý sinh viên
 * Chức năng:
 * 		- Lấy danh sách điểm từ giao diện
 * 		- Tính điểm trung bình của toàn bộ sinh viên
 * 		- Tìm điểm cao nhất và điểm cao nhì
 * 		- Sắp xếp danh sách điểm
 */

const gradeList = [];

 const getGradesFromUI = function(){
	 const tdTags = document.querySelectorAll("#tableSinhVien tbody tr td:nth-child(4)");
	 console.log(tdTags);

	 for(var i = 0; i < tdTags.length; i++){
		 gradeList.push(+tdTags[i].innerHTML);
	 }

	 console.log(gradeList);
 };

 const calcAverage = function(){
	 var sum = 0;
	 for (var i = 0; i < gradeList.length; i++){
		 sum += gradeList[i];
	 }
	 const average = sum / gradeList.length;
	 return average;
 };

 const findMaxAndSecond = function(){
	 var max = gradeList[0];
	 var second = 0;
	 for (var i = 1; i < gradeList.length; i++){
		 if (gradeList[i] > max){
			 second = max;
			 max = gradeList[i];
		 }
		 else if (gradeList[i] > second && gradeList[i] !== max){
			second = gradeList[i];
		 }
	 }
	 console.log('max: ', max);
	 console.log('second', second);
 };

 const sortGrades = function(){
	 for(var i = 0; i < gradeList.length; i++){
		for(var j = 0; j < gradeList.length; j++){
			if (gradeList[j] > gradeList[j+1]){
				var temp = gradeList[j];
				gradeList[j] = gradeList[j+1];
				gradeList[j+1] = temp;
			}
			console.log(gradeList);
		}
	 }
 }

 getGradesFromUI();

 calcAverage();

 findMaxAndSecond();

 sortGrades();